#!/bin/bash

set -e
git_root="$(git rev-parse --show-cdup)"

cd "$git_root"
echo "Running the script from git root directory"


printf "\n\nRunning style check\n\n\n"
flake8 \
    dias \
    --max-line-length 120 \
    --ignore E126,E127,E128,E121,E225,E226,E401,E402,E501,W291,W503,E731

printf "\n\n All stages finished succesfully\n\n"

cd -  > /dev/null # return back
