# Soft Attention over Knowledge Base

This directory contains a demo implementation of the soft attention
over a knowledge base as described by Wen (2018).

## How it works

The first input of the network is the current dialogue state matrix
`U_IN` (shape: hidden size × slot number) that is supposed to hold
information about what kind of answer the user wants to get and
information about what it is the user wants to know something
about. The second input is a knowledge base in form of a table.

Each cell in the table is embedded using an embedding matrix resulting
in a vector of fixed length. After that is the only place where a
weight matrix (WC) is used for the soft attention algorithm: The cell
vector is matrix-multiplied by a weight matrix to produce a vector of
length hidden size (same hidden size as in the dialogue state
matrix). All these cells vectors in a row are concatenated into a
matrix that has the same shape as the dialogue state matrix.

Since the dialogue state matrix and the cell row matrices now have the
same shape, they can be compared in a natural way using a metric based
on the cosine similarity. This way, similarity scores between the
dialogue state matrix and each cell row matrix are calculated. The
intuition is that if the dialogue state contains the information that
the user asked for a restaurant and some cell rows correspond to
restaurants, these matrices will be more similar since the previously
applied weight matrix learns to construct the cell row matrices in
exactly such a way.

The output matrix `U_KB` of the soft attention algorithm is then a
weighted average over the cell row matrices where the similarity
scores act as the weights. In a case where the similiarity scores make
it very clear which row is the one that is relevant for the dialogue
state, the values in `U_KB` will be dominated by that row.

Thus, as the output is basically a reweighing of the table, the
algorithm selects the important rows based on the dialogue state info
of what the user wants to know something about and, crucially,
**“pulls out” all the other information contained in that row, as
well,** because the rows are weighted as a whole and not by their
cells.

However, the output matrix contains lots of information about the
thing the user wants to know about without having information about
what the user actually asked for. This is why, the final output of the
algorithm is actually a concatenation of `U_IN` `U_KB`.

## An example with visualization

Consider the user query “What is the traffic at the nearest chinese restaurant?” and the following three knowledge base entries

```json
[
    {
        "distance": "5 miles",
        "traffic_info": "moderate traffic",
        "poi_type": "chinese restaurant",
        "address": "669 El Camino Real",
        "poi": "P.F. Changs"
    },
    {
        "distance": "6 miles",
        "traffic_info": "moderate traffic",
        "poi_type": "shopping center",
        "address": "434 Arastradero Rd",
        "poi": "Ravenswood Shopping Center"
    },
    {
        "distance": "8 miles",
        "traffic_info": "heavy traffic",
        "poi_type": "chinese restaurant",
        "address": "593 Arrowhead Way",
        "poi": "Chef Chu's"
    }
]
```

Let’s make up a dialogue state. The supposed meaning of the indices is
described in the file `states.idx`. In this case, the non-zero entries
mean that the user asks about a close-by place, which is also a
restaurant and wants to know about the traffic. We can visualize it
like this:

![](vis_fixed_WC/dialogue_state.png)

Just by making up a suitable weight matrix instead of training it, we can obtain the following three matrices for the cell rows:

![](vis_fixed_WC/row_0.png)
![](vis_fixed_WC/row_1.png)
![](vis_fixed_WC/row_2.png)

It can be seen that the first of the cell row matrices is most similar
to the dialogue state and this is also apparent from the similarity
scores (there were more than the three rows in the table):

![](vis_fixed_WC/similarity_scores.png)

We end up with the following knowledge base matrix `U_KB`:

![](vis_fixed_WC/kb_matrix.png)