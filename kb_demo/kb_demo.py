#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from collections import defaultdict
import json
import os

import numpy as np
import tensorflow as tf

from visualization import visualize_matrix, visualize_probability_dist


STATE_ROWS = ['distance', 'traffic_info', 'poi_type', 'poi', 'address']
STATE_COLS = [str(i) for i in range(4)]


def make_visualizations(network, embedded_table, dialogue_state,
                        visualization_dir, state_rows=STATE_ROWS,
                        state_cols=STATE_COLS):
    """Run the model on the given table and state and save visualizations
    in the given directory.
    """
    os.makedirs(visualization_dir, exist_ok=True)
    hidden_table, kb_matrix, similarity_scores, answers = (
        network.verbose_model([
            np.expand_dims(embedded_table, axis=0),
            np.expand_dims(dialogue_state, axis=0),
        ])
    )

    fig = visualize_matrix(dialogue_state, state_rows, state_cols)
    fig.savefig(os.path.join(visualization_dir, 'dialogue_state.png'))

    for i, row_matrix in enumerate(hidden_table[0]):
        fig = visualize_matrix(row_matrix, state_rows, state_cols)
        fig.savefig(os.path.join(visualization_dir, 'row_{}.png'.format(i)))

    x_labels = [str(i) for i in range(len(similarity_scores[0]))]
    fig = visualize_probability_dist(similarity_scores[0], x_labels)
    fig.savefig(os.path.join(visualization_dir, 'similarity_scores.png'))

    fig = visualize_matrix(kb_matrix[0], state_rows, state_cols)
    fig.savefig(os.path.join(visualization_dir, 'kb_matrix.png'))


def embed_table(table, emb_dict):
    """KB table encoder.
    Return encoded KB with size (table_size, slot_num, embedding size)
    """
    row_num = len(table)
    slot_num = len(table[0])
    emb_dim = len(list(emb_dict.values())[0])
    embedded_table = np.zeros((row_num, slot_num, 2 * emb_dim))

    for i, entry in enumerate(table):
        for j, (col_name, cell_value) in enumerate(entry.items()):
            embedded_table[i][j] = np.concatenate(
                (emb_dict[col_name], emb_dict[cell_value])
            )
    return embedded_table


def read_json(json_filename):
    """Read json file as python dictionary.
    """
    with open(json_filename) as f:
        data = json.load(f)
    return data


def read_embeddings_dict_from_file(filename, use_defaultdict=True):
    """Read embedding dictionary from a file which contains the pair of
    'entity' in KB and its representative embedding.
    In real implementation we may use pre-trained embedding.
    """
    embeddings = {}
    emb_dim = None
    with open(filename) as f:
        for line in f:
            line = line.strip()
            word, weights_str = line.split('\t')
            weights = [float(w) for w in weights_str.split(' ')]

            # Sanity check: Are all embeddings of the same size?
            if emb_dim:
                if len(weights) != emb_dim:
                    raise ValueError(
                        'Differing embedding sizes in file {}'
                        .format(filename)
                    )
            else:
                emb_dim = len(weights)
                if defaultdict:
                    embeddings = defaultdict(lambda: np.zeros(emb_dim))

            embeddings[word] = np.array([float(w) for w in weights])
    return embeddings


class Network:

    def __init__(self, emb_size, hidden_size, slot_num, answer_num,
                 memory_size=10, train_WC=True):
        # This class assumes that the table is already embedded.
        # Usually, we would use an Embedding layer for this.
        # embedded_table shape == (table_size, slot_num, emb_size)
        # dialogue_state = u^IN. Shape == (slot_num, hidden_size)
        embedded_table = tf.keras.Input(shape=[None, slot_num, emb_size],
                                        name='embedded_table')
        dialogue_state = tf.keras.Input(shape=[slot_num, hidden_size],
                                        name='dialogue_state')

        self.WC = tf.keras.layers.Dense(hidden_size, use_bias=False,
                                        activation='tanh', trainable=train_WC,
                                        name='hidden_table')

        # KB table = hidden_table = C
        # hidden_table shape == (batch_size, table_size, slot_num, hidden_size)
        hidden_table = self.WC(embedded_table)

        # SIMILARITY SCORE
        # similarity_scores shape == (batch_size, table_size)
        # Similarity distribution between the k-th entry in KB table, C_k, and dialogue state repr (DSR) U^IN
        #   computed as sum of dot product of C_k,t and each DSR column t, u^IN_t, in which
        #   C_k,t shape == u^IN_t shape == (1, hidden_size)
        # Thus, the distribution is over the number of entry (table_size)
        # Softmax function is used to convert this distribution into probability distribution
        similarity_scores = tf.keras.activations.softmax(
            tf.einsum('btsh,bsh->bt', hidden_table, dialogue_state),
        )
        similarity_scores = tf.identity(similarity_scores,
                                        name='similarity_scores')

        # BUILDING INFORMATION MATRIX U^KB
        # Add new dimension for similarity_scores
        # Now similarity_scores shape == (batch_size, table_size, 1, 1)
        expanded_similarity_scores = tf.expand_dims(similarity_scores, axis=-1)
        expanded_similarity_scores = tf.expand_dims(expanded_similarity_scores,
                                                    axis=-1)
        # info_matrix shape == (slot_num, hidden_size)
        # same as u^IN
        kb_matrix = tf.keras.backend.sum(
            expanded_similarity_scores * hidden_table,
            axis=1
        )
        kb_matrix = tf.identity(kb_matrix, name='kb_matrix')

        # Concatenation of the dialogue state and the retrieved knowledge
        # U^CAT in Wen 2018
        state_and_kb = tf.concat([dialogue_state, kb_matrix], axis=1)

        # If we would actually re-implement Wen 2018 here, we would
        # now produce the memory matrix U here:
        #W_CAT = tf.get_variable('W_CAT', trainable=True)
        #memory_matrix = W_CAT @ state_and_kb

        # But we want to predict some answer directly at this step instead.
        # So we just build an output layer.
        output_layer = tf.keras.layers.Dense(answer_num, activation='sigmoid',
                                             name='answers')
        answers = output_layer(tf.keras.layers.Flatten()(state_and_kb))

        self.model = tf.keras.Model(inputs=[embedded_table, dialogue_state],
                                    outputs=answers)

        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(),
            loss=tf.keras.losses.BinaryCrossentropy(),
            metrics=[tf.keras.metrics.BinaryAccuracy()],
        )

        # Model that has more than one output. This is for creating
        # the visualizations.
        self.verbose_model = tf.keras.Model(
            inputs=[embedded_table, dialogue_state],
            outputs=[hidden_table, kb_matrix, similarity_scores, answers]
        )
        dummy_loss = tf.keras.losses.BinaryCrossentropy()
        self.verbose_model.compile(
            optimizer=tf.keras.optimizers.Adam(),
            loss={
                'hidden_table': dummy_loss, 'kb_matrix': dummy_loss,
                'similarity_scores': dummy_loss, 'answers': dummy_loss
            }
        )

    def train(self, embedded_table, dialogue_states, labels, epochs=500):
        tables = np.array([embedded_table] * dialogue_states.shape[0],
                          dtype=np.float32)
        self.model.fit(
            {'embedded_table': tables,
             'dialogue_state': dialogue_states},
            labels,
            batch_size=5,
            epochs=epochs
        )

    def evaluate(self, embedded_table, dialogue_states, labels):
        tables = np.array([embedded_table] * dialogue_states.shape[0],
                          dtype=np.float32)
        loss = self.model.evaluate(
            {'embedded_table': tables,
             'dialogue_state': dialogue_states},
            labels,
            batch_size=5,
        )
        return loss

    def set_WC(self, weights):
        self.WC.set_weights([weights])

def main(train_file, table_file, emb_file, epochs=500, visualization_dir=None):
    train = read_json(train_file)
    table = read_json(table_file)
    emb_dict = read_embeddings_dict_from_file(emb_file)
    embedded_table = embed_table(table, emb_dict)
    # print (embedded_table)

    WC = np.zeros((embedded_table.shape[-1], 4))
    WC[24, 1] = 1
    WC[31, 1] = 1
    WC[28, 3] = 1
    WC[29, 3] = 2
    WC[30, 3] = 3
    #print(embedded_table.shape)
    #print(embedded_table)
    #print(embedded_table[0])
    #print(embedded_table[1])
    #print(embedded_table[2])
    #print(embedded_table[0] @ WC)
    #print(embedded_table[1] @ WC)
    #print(embedded_table[2] @ WC)
    #print(np.array(train[1]['state']))
    #print(train[1])

    embedded_table = embedded_table.astype(np.float32)
    dialogue_states = np.array([item['state'] for item in train],
                               dtype=np.float32)
    labels = np.array([item['label'] for item in train], dtype=np.int64)

    network = Network(emb_size=embedded_table.shape[-1],
                      hidden_size=dialogue_states.shape[-1],
                      slot_num=dialogue_states.shape[-2],
                      answer_num=labels.shape[-1],
                      train_WC=True)
    #network.set_WC(WC)

    network.train(embedded_table, dialogue_states, labels, epochs=epochs)
    print(network.evaluate(embedded_table, dialogue_states, labels))

    if visualization_dir:
        make_visualizations(network, embedded_table, dialogue_states[1],
                            visualization_dir)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('train_file')
    parser.add_argument('table_file')
    parser.add_argument('emb_file')
    parser.add_argument('--epochs', type=int, default=500)
    parser.add_argument('--visualization-dir')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    ARGS = parse_args()
    main(**vars(ARGS))
