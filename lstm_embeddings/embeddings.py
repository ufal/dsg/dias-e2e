#!/usr/bin/env python3
import numpy as np
import tensorflow as tf
import os
import sys
import urllib.request
from zipfile36 import ZipFile

"""
This example uses pre-trained word embeddings as obtained by GloVe model (https://nlp.stanford.edu/projects/glove/).
There are multiple versions of available word embeddings summarized in below:

                  Wikipedia 2014 + Gigaword 5 (6B tokens, 400K vocab, uncased, 50d, 100d, 200d, & 300d vectors, 822 MB download)
                  Common Crawl (42B tokens, 1.9M vocab, uncased, 300d vectors, 1.75 GB download)
                  Common Crawl (840B tokens, 2.2M vocab, cased, 300d vectors, 2.03 GB download)
                  Twitter (2B tweets, 27B tokens, 1.2M vocab, uncased, 25d, 50d, 100d, & 200d vectors, 1.42 GB download)

This version uses the most basic version Wikipedia 2014 but adaptation to bigger dataset should be straightforward.

There are four options for embedding dimmension options: 50, 100, 200 and 300 and we can specify the desired one in class constructor.

This module downloads the data and prepares the embedding dictionary. There are also some toy data prepared so that we can test the
functionality in modelu toy_embedded_lstm
"""


class Embeddings:
    def __init__(self, embedding_dim):

        self._URL = "http://nlp.stanford.edu/data/glove.6B.zip"
        path = os.path.basename(self._URL)
        self.embedding_dim = embedding_dim
        file_name = f"glove.6B.{self.embedding_dim}d.txt"

        # In case we do not have the embeddings downloaded we will get it

        if not os.path.exists(path):
            print(
                f"Getting the {path} dataset. Get yourself a cup of tee in a meantime as it takes quite a while to download", file=sys.stderr)
            urllib.request.urlretrieve(self._URL, filename=path)
        else:
            print("Data already downloaded")

        # Create a ZipFile Object and load downloaded embeddings in it

        self.embedding_dict = {}

        print(
            f"Extracting requested dataset {file_name} and preparing embedding dictionary")

        with ZipFile(path, "r") as obj:

            data = open(obj.extract(file_name))

            for line in data:

                word_num = line.split()

                if word_num[0] not in self.embedding_dict:

                    self.embedding_dict[word_num[0]] = word_num[1:]

            data.close()

    def prepare_data(self, data, size):

        # good default for max_lenght is 100
        # This function will map our training data to the pre-trained embeddings
        t = tf.keras.preprocessing.text.Tokenizer()
        t.fit_on_texts(data)
        # number of unique words
        vocab_size = len(t.word_index) + 1
        # integer encode the documents
        encoded_docs = t.texts_to_sequences(data)
        # pad documents to a max length of max sentence words
        padded_docs = tf.keras.preprocessing.sequence.pad_sequences(
            encoded_docs, maxlen=size, padding='post')
        embedding_matrix = np.zeros((vocab_size, 100))

        # Here we fill in the embeddings if they exist, else we leave zeros
        for word, ind in t.word_index.items():

            if self.embedding_dict.get(word) is not None:
                # Find the embedding in a dictionary
                embedding_matrix[ind] = self.embedding_dict.get(word)

        return embedding_matrix, padded_docs, vocab_size

    def load_toy_train_data(self):

        # Here are just some toy data to try out our network. The task is to predict the sentiment
        # label of each sentence.

        features = ['Well done!',
                    'Good work',
                    'Great effort',
                    'nice work',
                    'Excellent!',
                    'This is just perfect!',
                    'Cool',
                    'Not so bad',
                    'Disgrace',
                    'Can be done better',
                    'Needs improvement',
                    'Weak',
                    'Poor effort!',
                    'not good',
                    'poor work',
                    'Could have done better.',
                    'Quite a nice work',
                    'Some improvement is still needed',
                    'You should try harder next time',
                    'The best job I have seen!',
                    'This is just perfect',
                    'I am not very impressed']

        # This is perhaps not the best way how to do it
        longest = 0
        for sentence in features:
            if len(sentence.split()) > longest:
                longest = len(sentence.split())

        labels = np.array([1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                           0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0])

        return features, labels, longest
