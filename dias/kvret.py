#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import namedtuple
import logging
import json
from typing import Any, Callable, Iterable, List, Optional, Set

import numpy as np
import tensorflow as tf


def dialogue_intent_is_navigate(js: dict) -> bool:
    return js['scenario']['task']['intent'] == 'navigate'


def dialogue_intent_is_schedule(js: dict) -> bool:
    return js['scenario']['task']['intent'] == 'schedule'


def dialogue_intent_is_weather(js: dict) -> bool:
    return js['scenario']['task']['intent'] == 'weather'


def text_to_tokens(text: str) -> List[str]:
    return tf.keras.preprocessing.text.text_to_word_sequence(text)


def pad(iterable: Iterable, pad_value: Any,
        max_len: Optional[int] = None) -> np.ndarray:
    if not max_len:
        max_len = max(len(i) for i in iterable)
    return np.stack([
        np.pad(val, (0, max_len - len(val)),
               mode='constant', constant_values=pad_value)
        for val in iterable
    ])


def get_tokens_in_dataset(js: List[dict]) -> Set[str]:
    tokens = set()
    for dialogue in js:
        for turn in dialogue['dialogue']:
            tokens.update(text_to_tokens(turn['data']['utterance']))
            if turn['turn'] == 'assistant':
                tokens.update(turn['data']['requested'].keys())
                for slot_val in turn['data']['slots'].values():
                    tokens.update(text_to_tokens(slot_val))
        tokens.update(dialogue['scenario']['kb']['column_names'])
        rows = dialogue['scenario']['kb']['items']
        if rows:
            for row in rows:
                for col_val in row.values():
                    tokens.update(text_to_tokens(col_val))
    return tokens


DialogueSlice = namedtuple('DialogueSlice',
                           ['concatenated_utterances', 'requested', 'slots',
                            'kb', 'next_assistant_utterance'])

FullDialogueBatch = namedtuple('FullDialogueBatch',
                               ['utterances', 'requested', 'slots', 'kb'])
PartialDialogueBatch = namedtuple('PartialDialogueBatch',
                                  ['concatenated_utterances', 'requested',
                                   'slots', 'kb', 'next_assistant_utterance'])


class Vocabulary:
    PAD_IDX = 0
    UNK_IDX = 1
    BOS_IDX = 2
    EOS_IDX = 3
    SEP_IDX = 4
    NONE_IDX = 5
    PAD_TOKEN = '<pad>'
    UNK_TOKEN = '<unk>'
    BOS_TOKEN = '<bos>'
    EOS_TOKEN = '<eos>'
    SEP_TOKEN = '<sep>'
    NONE_TOKEN = '<none>'

    def __init__(self, tokens: Set[str]):
        self._i2t = [self.PAD_TOKEN, self.UNK_TOKEN, self.BOS_TOKEN,
                     self.EOS_TOKEN, self.SEP_TOKEN, self.NONE_TOKEN]
        self._i2t.extend(list(tokens))
        self._size = len(self._i2t)
        self._t2i = {self._i2t[i]: i for i in range(self._size)}

    def __len__(self) -> int:
        return self._size

    def i2t(self, i: int) -> str:
        if 0 <= i < self._size:
            return self._i2t[i]
        else:
            raise ValueError(
                'Invalid index {i} for vocabulary of size {size}'
                .format(i=i, size=self._size)
            )

    def t2i(self, t: str) -> int:
        if t in self._t2i:
            return self._t2i[t]
        else:
            return self.UNK_IDX

    def tokens_to_indices(self, tokens: List[str]) -> List[int]:
        return [self.t2i(t) for t in tokens]

    def indices_to_tokens(self, indices: List[int]) -> List[str]:
        return [self.i2t(i) for i in indices]


class Dialogue:

    INTENT_2_REQUEST_COLS = {
        'navigate': ['address', 'distance', 'poi', 'poi_type',
                     'traffic_info'],
        'weather': ['date', 'weather_attribute', 'location'],
        'schedule': ['agenda', 'date', 'event', 'party', 'room', 'time'],
    }

    INTENT_2_KB_COLS = {
        'navigate': ['address', 'distance', 'poi', 'poi_type',
                     'traffic_info'],
        'weather': ['friday', 'location', 'monday', 'saturday', 'sunday',
                    'thursday', 'today', 'tuesday', 'wednesday'],
        'schedule': ['agenda', 'date', 'event', 'party', 'room', 'time'],
    }

    def __init__(self, raw_utterances, utterances, requested,
                 slots, kb, intent, vocabulary):
        self.raw_utterances = raw_utterances
        self.utterances = utterances
        self.requested = requested
        self.slots = slots
        self.kb = kb
        self.intent = intent
        self.vocabulary = vocabulary

    @property
    def request_cols(self) -> List[str]:
        return self.INTENT_2_REQUEST_COLS[self.intent]

    @property
    def kb_cols(self) -> List[str]:
        return self.INTENT_2_KB_COLS[self.intent]

    def __len__(self) -> int:
        return len(self.requested)

    def up_to(self, end: int) -> DialogueSlice:
        utt_end = end * 2 - 1
        concatenated_utterances = []
        for i, utt in enumerate(self.utterances[0:utt_end]):
            concatenated_utterances.extend(utt)
            if i < utt_end - 1:
                concatenated_utterances.append(self.vocabulary.SEP_IDX)
        concatenated_utterances = np.array(concatenated_utterances,
                                           dtype=np.int32)
        next_assistant_utterance = self.utterances[utt_end - 1]

        requested = self.requested[end - 1]
        slots = self.slots[end - 1]
        kb = self.kb

        return DialogueSlice(concatenated_utterances, requested, slots, kb,
                             next_assistant_utterance)

    @classmethod
    def from_js_object(cls, js: dict, vocabulary: Vocabulary) -> 'Dialogue':
        turn_num = len(js['dialogue'])
        intent = js['scenario']['task']['intent']
        request_cols = cls.INTENT_2_REQUEST_COLS[intent]
        kb_cols = cls.INTENT_2_KB_COLS[intent]

        raw_utterances = []
        utterances = []  # Will be of length turn_num.
        slots = []  # Will be of length turn_num / 2.
        requested = np.zeros((turn_num // 2, len(request_cols)),
                             dtype=np.bool)

        i = 0  # Counter for number of assistant turns.
        for turn in js['dialogue']:
            raw_utterances.append(turn['data']['utterance'])
            indices = vocabulary.tokens_to_indices(
                text_to_tokens(turn['data']['utterance'])
            )
            utterances.append(indices)
            if turn['turn'] == 'assistant':
                slots_i = []
                for j, col in enumerate(request_cols):
                    if col in turn['data']['slots']:
                        indices = vocabulary.tokens_to_indices(
                            text_to_tokens(turn['data']['slots'][col])
                        )
                        slots_i.append(indices)
                    else:
                        slots_i.append([vocabulary.NONE_IDX])

                    try:
                        requested[i][j] = turn['data']['requested'][col]
                    except IndexError as e:
                        assistant_turns = sum(
                            turn['turn'] == 'assistant'
                            for turn in js['dialogue']
                        )
                        driver_turns = sum(
                            turn['turn'] == 'driver'
                            for turn in js['dialogue']
                        )
                        raise ValueError(
                            'Problematic dialogue ({} assistant turns, {}'
                            ' driver turns): {!r}'
                            .format(assistant_turns, driver_turns,
                                    js['dialogue'])
                        ) from e

                slots.append(slots_i)
                i += 1

        if js['scenario']['kb']['items']:
            kb_list = [
                [vocabulary.tokens_to_indices(text_to_tokens(row[col]))
                 for col in kb_cols]
                for row in js['scenario']['kb']['items']
            ]
            kb_cell_max_len = max(len(cell) for row in kb_list for cell in row)
            kb = np.stack([
                pad(row, vocabulary.PAD_IDX, max_len=kb_cell_max_len)
                for row in kb_list
            ])
        else:
            kb = None

        return cls(raw_utterances=raw_utterances, utterances=utterances,
                   requested=requested, slots=slots, kb=kb, intent=intent,
                   vocabulary=vocabulary)


def dialogues_in_dataset(js: List[dict], vocabulary: Vocabulary,
                         filter_fn: Optional[Callable[[dict], bool]]
                         = None) -> List[Dialogue]:
    if not filter_fn:
        def filter_fn(_):
            return True

    dialogues = []
    for js_dia in js:
        if filter_fn(js_dia):
            try:
                dialogues.append(
                    Dialogue.from_js_object(js_dia, vocabulary)
                )
            except ValueError as e:
                logging.warn(e.args[0])
    return dialogues


class KVRET:

    class Dataset:
        def __init__(self, dialogues: List[Dialogue], vocabulary: Vocabulary,
                     shuffle_batches: bool, seed=42):
            self.dialogues = dialogues
            self.vocabulary = Vocabulary

            self.dialogue_count = len(self.dialogues)
            self.total_turn_count = sum(len(d) for d in self.dialogues)

            self.slice_specs = [
                (dialogue_idx, turn_idx)
                for dialogue_idx, dialogue in enumerate(self.dialogues)
                for turn_idx in range(1, len(dialogue) + 1)
            ]
            self.slice_spec_count = len(self.slice_specs)

            self._shuffler = (np.random.RandomState(seed)
                              if shuffle_batches else None)

        def full_dialogue_batches(self, size=1):
            permutation = (
                self._shuffler.permutation(self.dialogue_count)
                if self._shuffler else np.arange(self.dialogue_count)
            )

            while len(permutation):
                batch_size = min(size or np.inf, len(permutation))
                batch_perm = permutation[:batch_size]
                permutation = permutation[batch_size:]

                utterances_batch = [self.dialogues[i].utterances
                                    for i in batch_perm]
                utterance_max_len = max(
                    len(utterance)
                    for utterances_dialogue in utterances_batch
                    for utterance in utterances_dialogue
                )
                dialogue_max_len = max(
                    len(utterances_dialogue)
                    for utterances_dialogue in utterances_batch
                )
                utterances = np.stack([
                    np.pad(pad(utterances, self.vocabulary.PAD_IDX,
                               max_len=utterance_max_len),
                           ((0, dialogue_max_len - len(utterances)), (0, 0)),
                           mode='constant',
                           constant_values=self.vocabulary.PAD_IDX)
                    for utterances in utterances_batch
                ])

                max_assistant_turns = dialogue_max_len // 2
                requested_batch = [self.dialogues[i].requested
                                   for i in batch_perm]
                padded = [
                    np.pad(
                        requested_dialogue,
                        ((0, max_assistant_turns - len(requested_dialogue)),
                         (0, 0)),
                        mode='constant',
                        constant_values=self.vocabulary.PAD_IDX
                    )
                    for requested_dialogue in requested_batch
                ]
                requested = np.stack(padded)

                slots_batch = [self.dialogues[i].slots for i in batch_perm]
                slot_max_len = max(len(slot)
                                   for slots_dialogue in slots_batch
                                   for slots_turn in slots_dialogue
                                   for slot in slots_turn)
                slots = np.stack([
                    np.pad(
                        np.stack([
                            pad(slots_turn, self.vocabulary.PAD_IDX,
                                max_len=slot_max_len)
                            for slots_turn in slots_dialogue
                        ]),
                        ((0, max_assistant_turns - len(slots_dialogue)),
                         (0, 0),
                         (0, 0)),
                        mode='constant',
                        constant_values=self.vocabulary.PAD_IDX
                    )
                    for slots_dialogue in slots_batch
                ])

                if (len(self.dialogues) > 0
                        and self.dialogues[0].kb is not None):
                    kbs_batch = [self.dialogues[i].kb for i in batch_perm]
                    kb_cell_max_len = max(len(cell)
                                          for kb in kbs_batch
                                          for row in kb
                                          for cell in row)
                    kb_max_rows = max(len(kb) for kb in kbs_batch)
                    kbs = np.stack([
                        np.pad(
                            np.stack([
                                pad(row, self.vocabulary.PAD_IDX,
                                    max_len=kb_cell_max_len)
                                for row in kb
                            ]),
                            ((0, kb_max_rows - len(kb)), (0, 0), (0, 0)),
                            mode='constant',
                            constant_values=self.vocabulary.PAD_IDX
                        )
                        for kb in kbs_batch
                    ])
                else:
                    kbs = None

                batch = FullDialogueBatch(utterances, requested, slots, kbs)
                yield batch

        def partial_dialogue_batches(self, size=None):
            permutation = (
                self._shuffler.permutation(self.slice_spec_count)
                if self._shuffler else np.arange(self.slice_spec_count)
            )

            while len(permutation):
                batch_size = min(size or np.inf, len(permutation))
                batch_perm = permutation[:batch_size]
                permutation = permutation[batch_size:]

                dialogue_slice_specs = [self.slice_specs[i]
                                        for i in batch_perm]
                dialogue_slices = [
                    self.dialogues[dialogue_idx].up_to(turn_idx)
                    for dialogue_idx, turn_idx in dialogue_slice_specs
                ]

                concatenated_utterances = pad(
                    [ds.concatenated_utterances for ds in dialogue_slices],
                    self.vocabulary.PAD_IDX
                )

                requested = np.array([ds.requested for ds in dialogue_slices],
                                     dtype=np.bool)

                slots_batch = [ds.slots for ds in dialogue_slices]
                slot_max_len = max(len(slot)
                                   for slots_turn in slots_batch
                                   for slot in slots_turn)
                slots = np.stack([
                    pad(slots_turn, self.vocabulary.PAD_IDX,
                        max_len=slot_max_len)
                    for slots_turn in slots_batch
                ])

                next_assistant_utterance = pad(
                    [ds.next_assistant_utterance for ds in dialogue_slices],
                    self.vocabulary.PAD_IDX
                )

                if (len(self.dialogues) > 0
                        and self.dialogues[0].kb is not None):
                    kbs_batch = [ds.kb for ds in dialogue_slices]
                    kb_cell_max_len = max(len(cell)
                                          for kb in kbs_batch
                                          for row in kb
                                          for cell in row)
                    kb_max_rows = max(len(kb) for kb in kbs_batch)
                    kbs = np.stack([
                        np.pad(
                            np.stack([
                                pad(row, self.vocabulary.PAD_IDX,
                                    max_len=kb_cell_max_len)
                                for row in kb
                            ]),
                            ((0, kb_max_rows - len(kb)), (0, 0), (0, 0)),
                            mode='constant',
                            constant_values=self.vocabulary.PAD_IDX
                        )
                        for kb in kbs_batch
                    ])
                else:
                    kbs = None

                batch = PartialDialogueBatch(concatenated_utterances,
                                             requested, slots, kbs,
                                             next_assistant_utterance)
                yield batch

    def __init__(self, train_dialogues: List[Dialogue],
                 dev_dialogues: List[Dialogue],
                 test_dialogues: List[Dialogue],
                 vocabulary: Vocabulary):
        self.train = self.Dataset(train_dialogues, vocabulary,
                                  shuffle_batches=True)
        self.dev = self.Dataset(dev_dialogues, vocabulary,
                                shuffle_batches=False)
        self.test = self.Dataset(test_dialogues, vocabulary,
                                 shuffle_batches=False)
        self.vocab = vocabulary

    @classmethod
    def from_files(cls, train_file: str, dev_file: str, test_file: str,
                   filter_fn: Optional[Callable[[dict], bool]]
                   = None) -> 'KVRET':
        raw = {}
        with open(train_file) as f:
            raw['train'] = json.load(f)
        with open(dev_file) as f:
            raw['dev'] = json.load(f)
        with open(test_file) as f:
            raw['test'] = json.load(f)

        all_train_tokens = get_tokens_in_dataset(raw['train'])
        vocabulary = Vocabulary(all_train_tokens)

        train_dialogues = dialogues_in_dataset(raw['train'], vocabulary,
                                               filter_fn)
        dev_dialogues = dialogues_in_dataset(raw['dev'], vocabulary,
                                             filter_fn)
        test_dialogues = dialogues_in_dataset(raw['test'], vocabulary,
                                              filter_fn)

        return cls(train_dialogues, dev_dialogues, test_dialogues, vocabulary)
