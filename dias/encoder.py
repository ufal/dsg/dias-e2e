#!/usr/bin/env python3

import numpy as np
import tensorflow as tf


import kvret

class Network:
    def __init__(self, args, vocab_size, n_slots):
        class Encoder(tf.keras.Model):
            def __init__(self):
                super(Encoder, self).__init__()

                self.embeddings = tf.keras.layers.Embedding(input_dim=vocab_size, output_dim=args.embed_dim, mask_zero=True)
                self.rnn = tf.keras.layers.LSTM(units=args.rnn_dim, return_sequences=True, return_state=True, recurrent_initializer='glorot_uniform')

            def call(self, inputs, hidden):
                embeddings = self.embeddings(inputs)
                output, state = self.rnn(embeddings, initial_state=hidden)
                return output, state

            def initialize_hidden(self):
                return tf.zeros(args.batch_size, args.rnn_dim)

        class DialogueStateRepr(tf.keras.Model):
            # for one slot
            def __init__(self, units):
                super(DialogueStateRepr, self).__init__()
                self.W_A = tf.keras.layers.Dense(1)

            def call(self, values):
                # values = hidden state values from the encoder
                score = self.W_A(values)
                att_weight = tf.nn.softmax(score, axis=1)
                vec = att_weight * values
                vec = tf.reduce_sum(vec, axis=1)
                return vec

        class RequestedClassifier(tf.keras.Model):
            def __init__(self, units):
                super(RequestedClassifier, self).__init__()
                self.output = tf.keras.layers.Dense(1, activation=tf.nn.sigmoid)


        # TODO: define training setup
        self._model = Model()
        self._optimizer = tf.optimizers.Adam()
        self._loss = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        self._metrics_training = {"loss": tf.metrics.Mean(), "accuracy": tf.metrics.SparseCategoricalAccuracy()}
        self._metrics_evaluation = {"accuracy": tf.metrics.Mean()}
        self._writer = tf.summary.create_file_writer(args.logdir, flush_millis=10 * 1000)


    def train_batch():
        with tf.GradientTape() as tape:



            probabilities = self.model(inputs, training=True)
            loss = self._loss(tags, probabilities, mask)

    def train_epoch():
        pass

    def predict_batch():
        pass

    def predict():
        pass

    def evaluate_batch():
        pass

    def evaluate_epoch():
        pass



if __name__ == "__main__":
    import argparse
    import datetime
    import os
    import re

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", default=100, type=int, help="Batch size.")
    parser.add_argument("--embed_dim", default=300, type=int, help="Embedding dimension.")
    parser.add_argument("--epochs", default=5, type=int, help="Number of epochs.")
    parser.add_argument("--intent", default="weather", type=str, help="Select 'weather', 'navigate', or 'schedule'")
    # parser.add_argument("--max_sentences", default=5000, type=int, help="Maximum number of sentences to load.")
    parser.add_argument("--recodex", default=False, action="store_true", help="Evaluation in ReCodEx.")
    parser.add_argument("--rnn_dim", default=300, type=int, help="RNN cell dimension.")
    parser.add_argument("--threads", default=1, type=int, help="Maximum number of threads to use.")
    args = parser.parse_args()

    # Fix random seeds and number of threads
    np.random.seed(77)
    tf.random.set_seed(77)
    tf.config.threading.set_inter_op_parallelism_threads(args.threads)
    tf.config.threading.set_intra_op_parallelism_threads(args.threads)

    # Create logdir name
    args.logdir = os.path.join("logs", "{}-{}-{}".format(
        os.path.basename(__file__),
        datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S"),
        ",".join(("{}={}".format(re.sub("(.)[^_]*_?", r"\1", key), value) for key, value in sorted(vars(args).items())))
    ))

    # Load the data

    if args.intent == "weather":
        filter_fn = kvret.dialogue_intent_is_weather
        n_slots =
    elif args.intent == "schedule":
        filter_fn = kvret.dialogue_intent_is_schedule
    elif args.intent == "navigate":
        filter_fn = kvret.dialogue_intent_is_navigate

    data = kvret.KVRET.from_files("dataset/kvret_train_public.json" ,
                                    "dataset/kvret_dev_public.json" ,
                                    "dataset/kvret_test_public.json",
                                    filter_fn=filter_fn)
    vocab_size = data.vocab._size
    n_slots = len(data.train.dialogues[0].INTENT_2_REQUEST_COLS[args.intent])

    # Create the network and train
    network = Network(args,
                      num_source_chars=len(morpho.train.data[morpho.train.FORMS].alphabet),
                      num_target_chars=len(morpho.train.data[morpho.train.LEMMAS].alphabet))
    network.train(morpho, args)

    # Generate test set annotations, but in args.logdir to allow parallel execution.
    out_path = "lemmatizer_competition_test.txt"
    if os.path.isdir(args.logdir): out_path = os.path.join(args.logdir, out_path)
    with open(out_path, "w", encoding="utf-8") as out_file:
        for i, sentence in enumerate(network.predict(morpho.test, args)):
            for j in range(len(morpho.test.data[morpho.test.FORMS].word_strings[i])):
                lemma = []
                for c in map(int, sentence[j]):
                    if c == MorphoDataset.Factor.EOW: break
                    lemma.append(morpho.test.data[morpho.test.LEMMAS].alphabet[c])

                print(morpho.test.data[morpho.test.FORMS].word_strings[i][j],
                      "".join(lemma),
                      morpho.test.data[morpho.test.TAGS].word_strings[i][j],
                      sep="\t", file=out_file)
            print(file=out_file)
