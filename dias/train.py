#!/usr/bin/env python3
import kvret


def main():
    # data_split = kvret.KVRET.from_files('./dataset/kvret_train_public.json', './dataset/kvret_dev_public.json', './dataset/kvret_dev_public.json',
    #         filter_fn=kvret.dialogue_intent_is_navigate)  # this fails
    data_split = kvret.KVRET.from_files('./dataset/kvret_train_public.json', './dataset/kvret_dev_public.json', './dataset/kvret_dev_public.json',
            filter_fn=kvret.dialogue_intent_is_schedule)
    batches = [b for b in data_split.train.partial_dialogue_batches(size=10)]



if __name__ == '__main__':
    main()
