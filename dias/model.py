#!/usr/bin/env python3

import numpy as np
import tensorflow as tf
import tempfile
import os
import time
import uuid

import kvret

# used for serializing/deserializing of a single model
class ModelSerializer:

    # create a folder for the model and serialize it
    def __init__(self, rootDir, model):
        dirname = time.strftime("%Y%m%d-%H%M%S") + "-" + str(uuid.uuid4().hex)
        path = f"{rootDir}/{dirname}"
        os.makedirs(path)
        model.save(f"{path}/model.h5")
        self.path = path

    # serializes model's weight to a given directory and returns the path
    def serializeWeights(self, model, loss):
        truncatedLoss = str(round(loss, 3))
        # append a unique name to the weights to ensure there won't be two weights with the same name
        filename = f"{self.path}/{time.strftime('%Y%m%d-%H%M%S')}-{truncatedLoss}-{str(uuid.uuid4().hex)}.h5"
        model.save_weights(filename)
        return filename

    def deleteWeight(self, path):
        os.remove(path)
    
    def loadWeights(self, model, path):
        model.load_weights(path)
        return model


class Network:
    def __init__(self, args, vocab_size, n_slots):

        class DialogueStateRepr(tf.keras.layers.Layer):
            # for one slot
            def __init__(self):
                super(DialogueStateRepr, self).__init__()
                self.W_A = tf.keras.layers.Dense(1)

            def call(self, values):
                # values = hidden state values from the encoder
                score = self.W_A(values)
                att_weight = tf.nn.softmax(score, axis=1)
                vec = att_weight * values
                vec = tf.reduce_sum(vec, axis=1)
                return vec                

        class EarlyStopper():
            def __init__(self, patience, limit, serializer):
                self.patience = patience
                self.limit = limit
                self.serializer = serializer
                self.epochsNotImproved = 0
                self.bestLoss = 100
                self.bestWeights = [] # array of tuples (loss, path/to/weights)

            # returns True if stopping early
            def earlyStop(self, logs, model):
                loss = logs[0]
                if (self.bestLoss > loss):
                    self.bestLoss = loss
                    self.epochsNotImproved = 0
                    if (len(self.bestWeights) < self.limit):
                        self.bestWeights.append((loss, self.serializer.serializeWeights(model, loss)))
                    else: # check if the new loss is in the X best losses
                        self.bestWeights.sort(key=lambda tup: tup[0], reverse=True)
                        for index, item in enumerate(self.bestWeights, start=0): # find the biggest loss that is bigger than the new one
                            if(item[0] > loss): # the weights we're going to replace
                                self.serializer.deleteWeight(self.bestWeights[index][1])
                                newWeights = self.serializer.serializeWeights(model, loss)
                                self.bestWeights[index] = (loss, newWeights)
                                break
                else: 
                    self.epochsNotImproved += 1
                    print(f"Model has not improved for {self.epochsNotImproved} epochs.")

                return self.patience < self.epochsNotImproved

            def loadBestWeights(self, model):
                self.bestWeights.sort(key=lambda tup: tup[0])
                return self.serializer.loadWeights(model, self.bestWeights[0][1])

        inputs = tf.keras.layers.Input(shape=[None])
        enc_embeddings = tf.keras.layers.Embedding(input_dim=vocab_size, output_dim=args.embed_dim)(inputs)
        in_rnn = tf.keras.layers.Dropout(0.75)(enc_embeddings)
        enc_rnn = tf.keras.layers.LSTM(units=args.rnn_dim, return_sequences=True, recurrent_initializer='glorot_uniform')(in_rnn)
        enc_rnn = tf.keras.layers.Dropout(0.75)(enc_rnn)

        dsr = dict()
        output = dict()
        for i in range(n_slots):
            dsr[i] = DialogueStateRepr()(enc_rnn)
            output[i] = tf.keras.layers.Dense(1, activation=tf.nn.sigmoid, name='out_slot_'+str(i))(dsr[i])

        self.model = tf.keras.Model(inputs=inputs, outputs=[out for out in output.values()])
        self.model.summary()
        self.model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.001),
              loss=tf.keras.losses.BinaryCrossentropy(),
              metrics=[tf.keras.metrics.BinaryAccuracy()])

        self.stopper = EarlyStopper(args.patience, args.model_limit, ModelSerializer(args.model_dir, self.model))

    def train(self, data, args):
        for epoch in range(args.epochs):
            for batch in data.train.partial_dialogue_batches(size=args.batch_size):
                inputs = batch.concatenated_utterances
                targets = np.squeeze(batch.requested.astype(int))
                targets = np.transpose(targets).tolist()
                logs = self.model.train_on_batch(inputs, targets)
            
            print ("Epoch "+str(epoch+1)+"\nTrain loss:", logs[0], "-- slot accuracies:", logs[args.n_slots+1:])
            eval_logs = self.evaluate(data.dev, args)

            if (self.stopper.earlyStop(eval_logs, self.model)): 
                print ("Stopping early...")
                break

        return self.stopper.loadBestWeights(self.model)
    
    def evaluate(self, dataset, args):
        for batch in dataset.partial_dialogue_batches(size=dataset.total_turn_count):
            inputs = batch.concatenated_utterances
            targets = np.squeeze(batch.requested.astype(int))
            targets = np.transpose(targets).tolist()
            logs = self.model.test_on_batch(inputs, targets)
        print("Eval loss: ", logs[0], "-- slot accuracies:", logs[args.n_slots+1:])
        return logs



if __name__ == "__main__":
    import argparse
    import datetime
    import os
    import re

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", default=10, type=int, help="Batch size.")
    parser.add_argument("--embed_dim", default=200, type=int, help="Embedding dimension.")
    parser.add_argument("--epochs", default=100, type=int, help="Number of epochs.")
    parser.add_argument("--intent", default="weather", type=str, help="Select 'weather', 'navigate', or 'schedule'")
    parser.add_argument("--rnn_dim", default=200, type=int, help="LSTM cell dimension.")
    parser.add_argument("--patience", default=10, type=int, help="Number of epochs with no improvements after which training will be stopped.")
    parser.add_argument("--model_dir", default="../experiments", type=str, help="Location of saved models and their weights.")
    parser.add_argument("--model_limit", default=3, type=int, help="Number of best-performing weights saved during the training.")
    args = parser.parse_args()

    # Fix random seeds and number of threads
    np.random.seed(77)
    tf.random.set_seed(77)

    # Load the data
    if args.intent == "weather":
        filter_fn = kvret.dialogue_intent_is_weather
    elif args.intent == "schedule":
        filter_fn = kvret.dialogue_intent_is_schedule
    elif args.intent == "navigate":
        filter_fn = kvret.dialogue_intent_is_navigate

    data = kvret.KVRET.from_files("dataset/kvret_train_public.json" ,
                                    "dataset/kvret_dev_public.json" ,
                                    "dataset/kvret_test_public.json",
                                    filter_fn=filter_fn)

    # Create the network and train
    args.n_slots = len(data.train.dialogues[0].INTENT_2_REQUEST_COLS[args.intent])
    network = Network(args,
                      vocab_size = data.vocab._size,
                      n_slots = args.n_slots)
    network.train(data, args)
    # network.evaluate(data.dev, args)
