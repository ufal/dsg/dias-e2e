This folder contains scripts for downloading  the [dataset](https://nlp.stanford.edu/blog/a-new-multi-turn-multi-domain-task-oriented-dialogue-dataset/) used in the original paper and creation of appropriate TensorFlow 2.0 `tf.data.Dataset` object. 

The `download.py` file contains a script for downloading and extracting the dataset.

in the `dataset.py` file there's a class `DatasetWrapper()`, which contains methods for returning vectorized test, dev and train datasets, as well as a vector dictionary. The dictionary is shared between the sets. 

It also seems like the `stanford.edu` site which hosts the dataset employs some kind of request limiting, since after few downloads, you might get 403 Forbidden error when attempting to download the dataset.