#!/usr/bin/python3

import json
import sys
import tensorflow as tf
from download import download_and_unzip_datasets


class DatasetWrapper:

    # Characters to be removed from utterances.
    STRIPPED_CHARACTERS = ['.', ',', '?', '!']

    def __init__(self):

        # Initialize instance variables
        self.ROOT_DIR = sys.path[0]
        self.DEV_FILE = self.ROOT_DIR + "/kvret_dev_public.json"
        self.TEST_FILE = self.ROOT_DIR + "/kvret_test_public.json"
        self.TRAIN_FILE = self.ROOT_DIR + "/kvret_train_public.json"
        self.vocabulary = {"_EOS": 0, "_OOV": 1}

        self.TEST_DATASET = self.__vectorize_datasets(self.__load_dialogues(self.TEST_FILE), self.vocabulary)
        self.DEV_DATASET = self.__vectorize_datasets(self.__load_dialogues(self.DEV_FILE), self.vocabulary)
        self.TRAIN_DATASET = self.__vectorize_datasets(self.__load_dialogues(self.TRAIN_FILE), self.vocabulary)

    def __load_dialogues(self, data_file):

        # Ensure the dataset is downloaded and unzipped.
        download_and_unzip_datasets()

        with open(data_file) as json_file:
            data = json.loads(json_file.read())
            dialogues = []
            for item in data:
                conversation = []
                for dialogue in item["dialogue"]:
                    utterance = dialogue.get("data").get("utterance")
                    for ch in self.STRIPPED_CHARACTERS:
                        utterance = utterance.replace(ch, "")
                    conversation.append(utterance)
                dialogues.append(conversation)
        return dialogues

    def __vectorize_datasets(self, dialogues, vector_dict):
        dict_index = len(vector_dict)  # Lowest unused number
        vectorized_dialogues = []
        for dialogue in dialogues:
            vectorized_dialogue = []
            for sentence in dialogue:
                for word in sentence.split():
                    if word not in vector_dict:
                        vector_dict[word] = dict_index
                        dict_index += 1
                    vectorized_dialogue.append(vector_dict[word])
                vectorized_dialogue.append(vector_dict["_EOS"])
            vectorized_dialogues.append(vectorized_dialogue)
        return vectorized_dialogues

    # The dataset contains a list of vectorized dialogues. Each dialogue is a list of integers.
    # Mapping of integers to words can be obtained by calling the get_vector_dictionary() function.

    # Train dataset
    def get_train_dataset(self):
        dialogues = self.TRAIN_DATASET
        return tf.data.Dataset.from_generator(lambda: dialogues, output_types=tf.int32, output_shapes=([None]))

    # Dev dataset
    def get_dev_dataset(self):
        dialogues = self.TRAIN_DATASET
        return tf.data.Dataset.from_generator(lambda: dialogues, output_types=tf.int32, output_shapes=([None]))

    # Test dataset
    def get_test_dataset(self):
        dialogues = self.TRAIN_DATASET
        return tf.data.Dataset.from_generator(lambda: dialogues, output_types=tf.int32, output_shapes=([None]))

    # Mapping dictionary. During the parsing, dict[word] -> int mapping is used, but during decoding dict[int] -> word is needed.
    # Thus, simply reverse the keys and values in the vocabulary dict, since their values are unique.
    def get_vector_dictionary(self):
        return {v: k for k, v in self.vocabulary.items()}
