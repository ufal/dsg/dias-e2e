## End to End Dialogue System - Project Plan

#### Introduction

I would suggest to first try to replicate the main ideas in the paper and once we have the pipeline successfully running we can try experimenting with new designs and architecture components. The below is my attempt to schedule the project process from bigenning to the end. The schedule is structured in chronological order, each section has a date and duration of the project phrase attatched to it. Each section has a subsections with tasks that I believe can be run in parallel and estimated number of people needed for each of this subtasks and description of its most crutial bullet points is also included.

#### 0) during lab 9.4.2019 
**_Preparation_**: everyone
- decide which framework to use for example Tensorflow 2.0/Pytorch
- decide which dataset we wil primarily use (preferably one mentioned in a paper so that we can compare the performance)
- how will the model training be done (will we have access to some external cluster/GPU that we can laverage?)
- decide organization of a project for example where it will be hosted Gitlab/Github
- divide work for the first stage
- brainstorm personal work preferences of people involved in project (someone may enjoy database work more than building a model and vice versa) so that the work is distributed to follow the preferences as closely as possible.

#### 1) 1 week (9.4.2019 - 16.4.2019)
**_Dataset_**: 1-2 people
- prepare load function that loads the dataset
- create the KB table and ensure that it is in the same format as described in the paper

**_Word embedding_**: 2 people
- come up with a model architecture that will do word embeddings

**_LSTM_**: 2 people 
- come up with architecture that will take word embeddings and maps them to hidden representations

**_Testing_**: everyone
- at this point we should test that the pipeline KB -> word embedding -> LSTM works properly

#### 2) 2-3 weeks (16.4.2019 - 7.5.2019)

- In my opinion this part could take us 2 weeks in total, however, it is in time of Eastern holiday when many people want to go away and therefore I would set aside additional week for this part of a project.

**_Dialogue state representation_**: 2-3 people
- compute group distributed representations of dialogue states
- encode the KB table
- soft attention over the KB entries
- obtain matrix U which is the fixed size memory representation over dialogue history and knowledge base

**_Decoder_**: 2-3 people
- input attention
- memory attention
- probability of generating a word
- omit RL part at this stage

**_Testing_**
- put together dialogue state representation and decoder

#### 3) 1 week (7.5.2019 - 14.5.2019)
**_First full testing_**: everyone
- putting all components together and testing whether everything works properly
- try different hyperparameters and compare results with authors of the paper
- brainstorm further project direction

#### 4) 2 weeks (14.5.2019 - 28.5.2019)
**_Model improvements_**: individually or in pairs
- divide team into small subteams (pairs or individuals) and each try some different approach to improve the model
- for example we can try to add the RL part mentioned in the paper or come up with some novel architecture upgrades

#### 5) 2 weeks (28.5.2019 - 10.6.2019)
**_Paper draft_**: everyone
- decide which is the best architecture and write a scintific paper about it
- I suggest using Overleaf for writing the paper
- review the work and conclude the project
- this can be possibly a soft deadline as there is so far no paper-deadline date for potential submition. We can work on the project even during rest of June or even summer if we have at this stage enough motivation and ideas to continue.

#### Notes on work during project
- communicate effectively the progress and obstackles via Slack and during labs (this is importanet also due to the fact that we will miss 2 Wednesdays in May due to state holiday)
- during project additional literature should be studied, which is relevant to the current project stage. We can make some running set of notes, where each of us will write down some useful facts/tricks that he or she founds along the way. This could also help us when writing the paper.
- it is important to communicate well the time constraints that people have during the project. That way we can more effectively reassign the work and substitute for each other when needed in order to stick to the schedule as closely as possible.
- check the work schedule regurarly and in case that we are falling behind try to detect the possible causes of it (time shcedule was too optimistic/difficulties during implementation/some other problems)

#### Project summary

**_Total estimated time spend on project_**: 10 weeks with 80% chance

**_Total people involved in project_**: 5-6

**_Project expected outcomes and goals_**: try to build our own functional end-to-end dialogue system architecture, experiment with it and try to publish a paper if the above succeeds. Learn from each other, share ideas and learn to collaborate together on bigger project. Have fun along the way!

