# Seq-to-seq Project Plan

The following is a rough outline of a project plan aimed towards replicating (and hopefully improving!) results presented in [this](https://arxiv.org/pdf/1806.04441.pdf) whitepaper. The project has two main goals:  implement a framework as proposed in the paper and in case we manage to achieve comparable results to the paper, a new paper summarizing our results along with any improvements should be produced and hopefully published, at least on arXiv and/or similar sites.

The project structure is divided into segments, roughly corresponding to the individual parts of the auto-dialabel framework suggested by the whitepaper. Due to the nature of the framework, where each part feeds into another one, working in parallel on multiple parts won't probably be viable, until at the very end during the fine-tuning process. Tasks that share the same segment are generally able to be completed in parallel.

# Preparations

The individual blockers are from top to bottom, ie. tasks from `Understanding the paper` should be done before doing tasks from `Development workflow`.

## Communications
Because of Easter and some holidays, opportunities to discuss stuff face to face during the labs will be rare. Thus, an efficient digital environment for collaborating must be established. A WhatsApp group for discussing labs homework topics already exists, but the nature of such chats isn't really sufficient for projects of such scale.

* Prepare Slack environment, either by reusing the existing channel `ufaldsg.slack.com` or creating a new server. The former is preferred, because a lot of people have already joined the server, along with other knowledgeable people from UFAL. (1h)

* Analyze existing solutions for group voice calls with screen sharing and/or code collaborations and find tutorials on how to use them. Google Hangouts Meet and Visual Studio Code should provide sufficient tools for group calls and collaboration, respectively. (1h)

## Understanding the paper

* Read the actual paper and try to understand it as much as possible. Make a short summary commenting on how the possible solutions could look like. (4h)

* Choose a proper framework and tools necessary for completing the goals. This requires a thorough analysis by a person who is familiar with the paper's topic, in order to mitigate the chance of choosing the wrong tools. (6h)

* Choose and prepare test datasets. Check if the ATIS dataset used by authors of the original paper is appropriate. Explore other possible baseline systems for results comparison than the ones used in the paper. (4h)

## Development Workflow
Development workflow must also be considered. This involves choosing the collaboration platform, deciding whether or not to utilize Agile principles, setting up proper CI/CD, code reviewing practives and many other things.

* Choose the collaboration platform (most probably GitHub or GitLab). Analyze the support of CI/CD practices on these platforms. Analyze which [workflow](https://medium.com/@patrickporto/4-branching-workflows-for-git-30d0aaee7bf) (if any) will be the best fit for the project. (2h)

* Choose project management platform. This can be either the issue tracking systems embedded in the collaboration platform, or an external one, like Redmine or Jira.

* Analyze if and how are Agile decelopment practices usable for the project. This also involves deciding whether or not to do code reviews and mandatory unit tests. (2h)

* Analyze possible usage of Docker, which would make sharing the development environment a lot easier, along with CI/CD support. (1h)

* If Docker will be utilized, prepare a sample Dockerfile for development, with source code being mounted from git repository on the host machine. Write a short tutorial how to use the Dockerfile. (4h)

# Development

At this point the team should have a pretty clear idea what must be done. A proper communication and collaboration platforms are set up, as well as the development environment. Collaboration guidelines are also set up. For each subset, the work should be divided using the chosen methodology (Agile, waterfall...).

Ideally, each subsection should take 1 week. At the start of each development week, a grooming session should be done, where the week's problem will get breaken down to individual subtasks with explicitly defined subgoals. The grroming should be led by the most knowledgeable people. During periodic checkups the team members should state what they're currently working on and what's blocking them, to ensure the team as a whole is properly informed.

The time estimations are also intentionally vague in case Agile methodology and it's Sprints will be used. Individual blockers will be determined during the breakdown of stories.

## Encoder

* Analyze avaliable solutions and implement the most appropriate one. (1w)

## Dialogue state representation

* Analyze avaliable solutions and implement the most appropriate one. (1w)

## KB representation

* Analyze avaliable solutions and implement the most appropriate one. (1w)

## KB

* Analyze avaliable solutions and implement the most appropriate one. (1w)

* The knowledge base can be mocked, so it doesn't have to be actually implemented.

## Decoder

* Analyze avaliable solutions and implement the most appropriate one. (1w)


## Fine Tuning and Testing

* After the majority of the project is done, additional fine tuning of various parameters and possibly analyzing performance bottlenecks should be done. (1w)

* Test the project and fix any bugs, should they occur. (3d)

# Paper

At this point all work on the project should be finished and the results properly evaluated.

* Produce a paper for publishment summarizing the results and steps that led to obtaining them. (1w)