# Project plan for reimplementing end-to-end task oriented dialogue system

## Goals
- Re-implementation the **core part** of the system in paper: https://arxiv.org/pdf/1806.04441.pdf
- Try collaboration on time
- Publish a report/paper

## Long-term plan (Goals)
1. Implement individual components and verify their functionality on toy problems / unit-tests
2. Build end-to-end training/evaluation loop and setup experimental setup
3. Write a report about architecture (the implementation) and the experiments

### Collaboration and work management
- DONE(2h, high priority) Git repository with CI, READMES, Contribution guidelines (2h)
- DONE(30m, middle priority) Setup communication channel - slack
- (1h, top priority) prepare plan - tasks for first two weeks and high level plan for the whole semester
- (1h top priority) discuss remote work and collaboration over slack - Eastern

### High-level design & slow start

1. Decide the framework, e.g. TensorFlow/PyTorch
- DONE (30m, Top priority) Strongly suggest Tensorflow 2.0 (alpha)/Tensorflow 2.0
- (30m, middle priority) which concepts needs to be explained?
    - `Training loop`
    - `Gradient/Gradient descent` intuition
    - `Embedding`
    - `Encoder`
    - `seq2seq` aka `Encoder-Decoder`
    - `Attention`
    - `Dataset` and dataset loading in the framework
    - `Git(lab) workflow`
    - `CI`
- (2h, middle priority) Explaining and studying the concepts
- (10h, top priority) Understand the paper https://arxiv.org/abs/1806.04441. Especially the KB attention.

### Implementation immediate plan

See tasks in Gitlab. Below are the names of tasks discovered from project plans or created during a lab.
- (top priority, Vojta + Oplatek) Load [KWRET dataset](https://nlp.stanford.edu/blog/a-new-multi-turn-multi-domain-task-oriented-dialogue-dataset/) so it can be used later in training pipeline
    - (1h) find/decide the right splits
    - (2h) decided the right format
        - training example format
        - embedding/vocabulary extraction
        - caching, shuffling
- (3h, top priority, Oplatek) implement training loop
    - depends on loading dataset
    - integrate training loss ([crossentropy](todo cite))
    - integrate evaluation metrics - accuracy
    - stopping criterion
    - include end-to-end test on small dataset
- (1h, middle priority, Oplatek) Integrate F1-score evaluation into training loop
    - depends on training loop
- (30m, high priority, Oplatek) Implement CI and demonstrate test
    - e.g. on for training or any other task
- (4h, middle priority) Use pre-trained embeddings
    - select the right pre-trained model
    - load the model to the TensorFlow 2.0 and initialize the embeddings
    - fine-tune the initialized embeddings - test it with LSTM Language modelling (LM) - todo link tutorial
- (10h, top-priority, May, Simon) -
    - Explanation how KB attention works
    - Plan how to implement it - write a snippet or pseudo-code
    - Oplatek only review (explanation backup)
- (6h, high-priority, Janus) - Encoder for dialogue
    - Use LSTM to encode a dialogue history - sequence of words with a flag whether it belongs to user or system into fixed embedding
    - First, use the [LM tutorial](todo link) and instead of user/system flag use flag for words if they start with lower-case.
